/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.javaswingcomponent;

/**
 *
 * @author Acer
 */
import javax.swing.*;  
  
public class JRootPaneExample {  
     public static void main(String[] args) {  
            JFrame f = new JFrame();  
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
            JRootPane root = f.getRootPane();  
  
            
            JMenuBar bar = new JMenuBar();  
            JMenu menu = new JMenu("File");  
            bar.add(menu);  
            menu.add("Open");  
            menu.add("Close");  
            root.setJMenuBar(bar);  
  
            
            root.getContentPane().add(new JButton("Press Me"));  
  
            
            f.pack();  
            f.setVisible(true);  
          }  
}  